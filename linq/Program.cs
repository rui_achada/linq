﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LivrariaLinq;

namespace linq
{
    class Program
    {
        static void Main(string[] args)
        {
            // não é preciso instanciar (new) porque o método é estático, assim fica com a lista que já está nos alunos
            List<Aluno> Alunos = ListaDeAlunos.LoadAlunos();

            //Ordenar lista por apelido
            //Alunos = Alunos.OrderBy(x => x.Apelido).ToList();
            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ToList();

            // caso o apelido seja o mesmo, vai ordenar pelas disciplinas feitas
            //Alunos = Alunos.OrderByDescending(x => x.Apelido).ThenByDescending(x => x.DisciplinasFeitas).ToList();

            // pesquisa ao aluno
            //Alunos = Alunos.Where(x => x.DisciplinasFeitas > 10 && x.DataNascimento.Month == 3).ToList();

            foreach (var aluno in Alunos)
            {
                Console.WriteLine($"{aluno.PrimeiroNome}   {aluno.Apelido}   {aluno.DataNascimento.ToShortDateString()}    Disciplinas feitas: {aluno.DisciplinasFeitas}");
            }

            int totalDeDisplinasFeitas = Alunos.Sum(x => x.DisciplinasFeitas);
            double mediaDeDisplinasFeitas = Alunos.Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas: {totalDeDisplinasFeitas}");
            Console.WriteLine($"Média de disciplinas feitas: {mediaDeDisplinasFeitas:N2}");

            totalDeDisplinasFeitas = Alunos.Where(x => x.DataNascimento.Month == 2).Sum(x => x.DisciplinasFeitas);
            mediaDeDisplinasFeitas = Alunos.Where(x => x.DataNascimento.Month == 2).Average(x => x.DisciplinasFeitas);

            Console.WriteLine($"Total de disciplinas feitas dos que nasceram em Feveiro: {totalDeDisplinasFeitas}");
            Console.WriteLine($"Média de disciplinas feitas dos que nasceram em Fevereiro: {mediaDeDisplinasFeitas:N2}");

            Console.ReadKey();
        }
    }
}
